/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 8.333333333333334, "KoPercent": 91.66666666666667};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.0, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.0, 500, 1500, "POST - Login"], "isController": false}, {"data": [0.0, 500, 1500, "DELETE - Seller Product {id}"], "isController": false}, {"data": [0.0, 500, 1500, "GET - Buyer Product"], "isController": false}, {"data": [0.0, 500, 1500, "GET - Buyer Product {id}"], "isController": false}, {"data": [0.0, 500, 1500, "PUT - Buyer Order"], "isController": false}, {"data": [0.0, 500, 1500, "GET - Buyer Order {id}"], "isController": false}, {"data": [0.0, 500, 1500, "POST - Buyer Order"], "isController": false}, {"data": [0.0, 500, 1500, "GET - Seller Product"], "isController": false}, {"data": [0.0, 500, 1500, "GET - Seller Product {id}"], "isController": false}, {"data": [0.0, 500, 1500, "GET - Buyer Order"], "isController": false}, {"data": [0.0, 500, 1500, "POST - Register"], "isController": false}, {"data": [0.0, 500, 1500, "POST - Seller Product"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 120, 110, 91.66666666666667, 5428.625, 0, 71209, 274.5, 1499.9000000000015, 65891.49999999991, 70955.52999999998, 1.5858751387640748, 775.5888335805756, 0.33452053708304696], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["POST - Login", 10, 10, 100.0, 334.7, 265, 429, 327.0, 427.7, 429.0, 429.0, 11.248593925759279, 3.8996590269966256, 3.0757874015748032], "isController": false}, {"data": ["DELETE - Seller Product {id}", 10, 10, 100.0, 383.9, 271, 1225, 296.0, 1133.0000000000005, 1225.0, 1225.0, 5.564830272676683, 1.9237792153589317, 1.2825194769059545], "isController": false}, {"data": ["GET - Buyer Product", 10, 0, 0.0, 61221.899999999994, 43768, 71209, 66920.0, 71088.3, 71209.0, 71209.0, 0.14027016032879325, 821.7967062374634, 0.02862935889523222], "isController": false}, {"data": ["GET - Buyer Product {id}", 10, 10, 100.0, 883.4000000000001, 253, 1524, 1034.5, 1499.9, 1524.0, 1524.0, 0.35014005602240894, 1.3431153711484594, 0.07283186712184875], "isController": false}, {"data": ["PUT - Buyer Order", 10, 10, 100.0, 0.0, 0, 0, 0.0, 0.0, 0.0, 0.0, 0.35516408580764314, 0.3898480785622958, 0.0], "isController": false}, {"data": ["GET - Buyer Order {id}", 10, 10, 100.0, 0.0, 0, 0, 0.0, 0.0, 0.0, 0.0, 0.35515147210285186, 0.389834233050396, 0.0], "isController": false}, {"data": ["POST - Buyer Order", 10, 10, 100.0, 393.3, 259, 871, 283.5, 861.4000000000001, 871.0, 871.0, 0.350754121360926, 0.12125679586110136, 0.09625186338126973], "isController": false}, {"data": ["GET - Seller Product", 10, 10, 100.0, 276.0, 260, 299, 272.5, 298.3, 299.0, 299.0, 12.269938650306749, 4.241756134969325, 2.516296012269939], "isController": false}, {"data": ["GET - Seller Product {id}", 10, 10, 100.0, 274.09999999999997, 261, 298, 271.5, 297.0, 298.0, 298.0, 11.976047904191617, 4.140157185628743, 2.502806886227545], "isController": false}, {"data": ["GET - Buyer Order", 10, 10, 100.0, 0.0, 0, 0, 0.0, 0.0, 0.0, 0.0, 0.35515147210285186, 0.38844692261249425, 0.0], "isController": false}, {"data": ["POST - Register", 10, 10, 100.0, 1105.8999999999999, 1000, 1831, 1023.5, 1754.5000000000002, 1831.0, 1831.0, 4.5167118337850045, 3.859494975158085, 3.1714021567299007], "isController": false}, {"data": ["POST - Seller Product", 10, 10, 100.0, 270.3, 261, 289, 269.5, 287.8, 289.0, 289.0, 12.40694789081886, 4.289120657568238, 2.786716811414392], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["400/Bad Request", 10, 9.090909090909092, 8.333333333333334], "isController": false}, {"data": ["500/Internal Server Error", 10, 9.090909090909092, 8.333333333333334], "isController": false}, {"data": ["403/Forbidden", 50, 45.45454545454545, 41.666666666666664], "isController": false}, {"data": ["401/Unauthorized", 10, 9.090909090909092, 8.333333333333334], "isController": false}, {"data": ["Non HTTP response code: java.net.URISyntaxException/Non HTTP response message: Illegal character in authority at index 8: https://{url}/buyer/order", 10, 9.090909090909092, 8.333333333333334], "isController": false}, {"data": ["Non HTTP response code: java.net.URISyntaxException/Non HTTP response message: Illegal character in authority at index 8: https://{url}/buyer/order/:id", 20, 18.181818181818183, 16.666666666666668], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 120, 110, "403/Forbidden", 50, "Non HTTP response code: java.net.URISyntaxException/Non HTTP response message: Illegal character in authority at index 8: https://{url}/buyer/order/:id", 20, "400/Bad Request", 10, "500/Internal Server Error", 10, "401/Unauthorized", 10], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": ["POST - Login", 10, 10, "401/Unauthorized", 10, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["DELETE - Seller Product {id}", 10, 10, "403/Forbidden", 10, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": ["GET - Buyer Product {id}", 10, 10, "400/Bad Request", 10, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["PUT - Buyer Order", 10, 10, "Non HTTP response code: java.net.URISyntaxException/Non HTTP response message: Illegal character in authority at index 8: https://{url}/buyer/order/:id", 10, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["GET - Buyer Order {id}", 10, 10, "Non HTTP response code: java.net.URISyntaxException/Non HTTP response message: Illegal character in authority at index 8: https://{url}/buyer/order/:id", 10, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["POST - Buyer Order", 10, 10, "403/Forbidden", 10, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["GET - Seller Product", 10, 10, "403/Forbidden", 10, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["GET - Seller Product {id}", 10, 10, "403/Forbidden", 10, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["GET - Buyer Order", 10, 10, "Non HTTP response code: java.net.URISyntaxException/Non HTTP response message: Illegal character in authority at index 8: https://{url}/buyer/order", 10, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["POST - Register", 10, 10, "500/Internal Server Error", 10, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["POST - Seller Product", 10, 10, "403/Forbidden", 10, "", "", "", "", "", "", "", ""], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
